import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:login_screen/LoginScreen/LoginScreen.dart';

import 'SplashScreen.dart';

var screenHeight = 0.0;
var screenWidth = 0.0;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    //Force Portrait mode
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      routes: <String, WidgetBuilder> {
        '/LoginScreen': (BuildContext context) => new LoginScreen(),
      },
    );
  }
}