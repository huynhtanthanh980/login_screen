import 'package:flutter/material.dart';

class CustomColor {
  static Color textFieldBackgound = Color.fromRGBO(82, 92, 164, 1.0);
  static Color buttonTopBackgound = Color.fromRGBO(82, 92, 164, 1.0);
  static Color buttonBottomBackgound = Color.fromRGBO(72, 86, 188, 1.0);
  static Color button = Color.fromRGBO(202, 174, 122, 1.0);
}
