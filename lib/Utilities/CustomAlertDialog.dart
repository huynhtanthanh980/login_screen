import 'package:flutter/material.dart';
import 'package:login_screen/main.dart';

showAlert(BuildContext context, String message, Function onClose) {
  //onclose() will be triggered first when this function is called
  showGeneralDialog(
    context: context,
    pageBuilder: (context, Animation<double> animation,
        Animation<double> secondaryAnimation) {},
    transitionBuilder: (context, Animation<double> animation1,
        Animation<double> animation2, widget) {
      return Transform.scale(
        scale: animation1.value,
        child: Opacity(
          opacity: animation1.value,
          child: AlertDialog(
            backgroundColor: Color.fromRGBO(202, 174, 122, 1.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(2.0),
            ),
            titlePadding: EdgeInsets.all(0.0),
            title: Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: FractionalOffset.topRight,
                      child: Container(
                        child: IconButton(
                          icon: Icon(
                            Icons.close,
                            color: Colors.white,
                          ),
                          iconSize: (12 / 406) * screenHeight,
                          onPressed: () => Navigator.pop(context),
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: (2 / 406) * screenHeight,
                          ),
                          Text(
                            message,
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontStyle: FontStyle.normal,
                              fontSize: (10 / 406) * screenHeight,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: (12 / 406) * screenHeight,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            contentPadding: const EdgeInsets.all(20.0),
          ),
        ),
      );
    },
    barrierColor: Colors.black.withOpacity(0.5),
    //black background outside the dialog
    barrierDismissible: true,
    //dialog can be pop out by click on the outside area
    barrierLabel: "",
    //must have if barrierDismissible = true ?????
    transitionDuration: Duration(milliseconds: 200),
  );
}