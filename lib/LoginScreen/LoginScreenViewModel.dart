import 'dart:convert';

import 'package:http/http.dart' as http;

class EmailCheckResult {
  final bool status;
  final String statusMessage;

  EmailCheckResult({
    this.status,
    this.statusMessage,
  });

  factory EmailCheckResult.fromJson(Map<String, dynamic> json) {
    return EmailCheckResult(
      status: json['status'],
      statusMessage: json['statusMessage'],
    );
  }
}

Future<EmailCheckResult> emailCheck(String emailLogin) async {
  final response = await http
      .post('http://sanofi.codev.vn/api/user/checkmail?emailLogin=$emailLogin');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return EmailCheckResult.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to check email');
  }
}
