import 'package:async_loader/async_loader.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:login_screen/ChangePasswordScreen/VerifyCodeScreen.dart';
import 'package:login_screen/LoginScreen/LoginScreenViewModel.dart';
import 'package:login_screen/LoginScreen/PasswordLoginScreen.dart';
import 'package:login_screen/Utilities/CustomAlertDialog.dart';
import 'package:login_screen/Utilities/CustomColor.dart';
import 'package:login_screen/main.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<LoginScreen> createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {

  final textFieldController = new TextEditingController();

  bool isLoading = false;

  final GlobalKey<AsyncLoaderState> _asyncLoaderState = new GlobalKey<
      AsyncLoaderState>();

  EmailCheckResult result;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery
        .of(context)
        .size;

    screenHeight = size.height;
    screenWidth = size.width;

    var asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getButtonContent(),
      renderLoad: () =>
          Container(
            height: (9 / 406) * screenHeight,
            width: (9 / 406) * screenHeight,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
      renderError: ([error]) =>
      new Text('Error'),
      renderSuccess: ({data}) {
        return Text(
          "Continue",
          style: TextStyle(
            fontSize: (9 / 406) * screenHeight,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );

    return Scaffold(

      resizeToAvoidBottomPadding: false,

      body: Container(
        height: screenHeight,
        width: screenWidth,
        child: Stack(
          children: <Widget>[
            Positioned(
              height: screenHeight,
              width: screenWidth,
              top: 0.0,
              child: Image.asset(
                'assets/LoginScreen.png',
                fit: BoxFit.fill,
              ),
            ),

            Align(
              alignment: Alignment(0, 0),
              child: Container(
                height: (62 / 406) * screenHeight,
                width: (154.2 / 187.5) * screenWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment(1.0, 0.0),
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30.5),
                                  bottomRight: Radius.circular(30.5),
                                ),
                                color: CustomColor.textFieldBackgound,
                              ),
                              child: Align(
                                alignment: Alignment(0.85, 0),
                                child: Icon(
                                  Icons.mail,
                                  color: Colors.white,
                                  size: (10 / 187.5) * screenWidth,
                                ),
                              ),
                            ),
                          ),

                          Container(
                            height: double.infinity,
                            width: (131.2 / 187.5) * screenWidth,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(30.5)),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(
                                    0.0,
                                    0.0,
                                  ),
                                  blurRadius: 10.0,
                                  spreadRadius: 0.0,
                                ),
                              ],
                            ),
                            child: TextField(
                              controller: textFieldController,
                              obscureText: false,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: (6 / 187.5) * screenWidth
                              ),
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Email address',
                                hintStyle: TextStyle(
                                    fontSize: (6 / 187.5) * screenWidth
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.5)),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [CustomColor.buttonTopBackgound, CustomColor
                              .buttonBottomBackgound
                          ],
                        ),
                      ),
                      child: Center(
                        child: Container(
                          height: double.infinity,
                          width: (107.7 / 187.5) * screenWidth,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(30.5)),
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(30.5)),
                            ),
                            color: CustomColor.button,
                            disabledColor: CustomColor.button,
                            onPressed: (isLoading) ? null : () {
                              onContinueButtonTap();
                            },
                            child: asyncLoader,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  getButtonContent() async {
    if (isLoading) {
      final emailAccount = textFieldController.text.toString();
      result = await emailCheck(emailAccount);
    }
  }

  onContinueButtonTap() async {
    final emailAccount = textFieldController.text.toString();

    if (emailAccount.isEmpty) {
      showAlert(context, "Please input email !", null);
    }
    else if (EmailValidator.validate(emailAccount)) {
      setState(() {
        isLoading = true;
      });

      _asyncLoaderState.currentState
          .reloadState()
          .whenComplete(() {
        setState(() {
          isLoading = false;
        });
        if (result.statusMessage == "User found!") {
          Navigator.push(context, MaterialPageRoute(
              builder: (context) => PasswordLoginScreen(email: emailAccount,)));
        }
        else {
          showAlert(context,
              "You are not registered ?\nPlease check your email to register !",
              navigateToSignUpScreen(emailAccount));
        }
      });
    }
    else {
      showAlert(context, "Please input a valid email !", null);
    }
  }

  navigateToSignUpScreen(String email) {
    Navigator.push(context, MaterialPageRoute(
        builder: (context) =>
            VerifyCodeScreen(
              email: email, isNewAccount: true,)));
  }
}