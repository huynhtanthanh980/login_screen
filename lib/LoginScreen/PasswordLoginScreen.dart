import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:login_screen/ChangePasswordScreen/VerifyCodeScreen.dart';
import 'package:login_screen/LoginScreen/PasswordLoginViewModel.dart';
import 'package:login_screen/Utilities/CustomAlertDialog.dart';
import 'package:login_screen/Utilities/CustomColor.dart';
import 'package:login_screen/logoutScreen.dart';

import '../main.dart';

class PasswordLoginScreen extends StatefulWidget {
  final String email;

  PasswordLoginScreen({Key key, @required this.email}) : super(key: key);

  @override
  State<PasswordLoginScreen> createState() => PasswordLoginScreenState(email);
}

class PasswordLoginScreenState extends State<PasswordLoginScreen> {
  String email;

  PasswordLoginScreenState(this.email);

  final textFieldController = new TextEditingController();

  bool isLoading = false;

  final GlobalKey<AsyncLoaderState> _asyncLoaderState = new GlobalKey<
      AsyncLoaderState>();

  LoginResult result;

  var passwordObscureText = true;
  var isTextFieldEmpty = true;

  @override
  void initState() {
    super.initState();

    textFieldController.addListener(() {
      setState(() {
        if (textFieldController.text.isEmpty) {
          isTextFieldEmpty = true;
          passwordObscureText = true;
        }
        else {
          isTextFieldEmpty = false;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getButtonContent(),
      renderLoad: () =>
          Container(
            height: (9 / 406) * screenHeight,
            width: (9 / 406) * screenHeight,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
      renderError: ([error]) =>
      new Text('Error'),
      renderSuccess: ({data}) {
        return Text(
          "Login",
          style: TextStyle(
            fontSize: (9 / 406) * screenHeight,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        height: screenHeight,
        width: screenWidth,
        child: Stack(
          children: <Widget>[
            Positioned(
              height: screenHeight,
              width: screenWidth,
              top: 0.0,
              child: Image.asset(
                'assets/LoginScreen.png',
                fit: BoxFit.fill,
              ),
            ),
            Align(
              alignment: Alignment(0, 0),
              child: Container(
                height: (126.7 / 406) * screenHeight,
                width: (154.2 / 187.5) * screenWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: (20 / 187.5) * screenWidth,
                      child: CircleAvatar(
                        backgroundColor: CustomColor.textFieldBackgound,
                        child: Icon(
                          Icons.person,
                          size: (15 / 406) * screenHeight,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        email,
                        style: TextStyle(
                          fontSize: (6 / 187.5) * screenWidth,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment(1.0, 0.0),
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(30.5),
                                  bottomRight: Radius.circular(30.5),
                                ),
                                color: CustomColor.textFieldBackgound,
                              ),
                              child: Align(
                                alignment: Alignment(0.85, 0),
                                child: Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                  size: (10 / 187.5) * screenWidth,
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(
                                      0.0,
                                      0.0,
                                    ),
                                    blurRadius: 10.0,
                                    spreadRadius: 0.0,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: <Widget>[
                                  TextField(
                                    controller: textFieldController,
                                    obscureText: passwordObscureText,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: (6 / 187.5) * screenWidth),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Password',
                                      hintStyle: TextStyle(
                                          fontSize: (6 / 187.5) * screenWidth),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment(0.9, 0.0),
                                    child: GestureDetector(
                                      onTapDown: (isTextFieldEmpty) ? null : (
                                          detail) {
                                        setState(() {
                                          passwordObscureText = false;
                                        });
                                      },
                                      onTapUp: (isTextFieldEmpty) ? null : (
                                          detail) {
                                        setState(() {
                                          passwordObscureText = true;
                                        });
                                      },
                                      onTapCancel: (isTextFieldEmpty)
                                          ? null
                                          : () {
                                        setState(() {
                                          passwordObscureText = true;
                                        });
                                      },
                                      child: passwordObscureText
                                          ? Icon(
                                        Icons.visibility,
                                        size: (10 / 187.5) * screenWidth,
                                        color: isTextFieldEmpty ? Colors.black
                                            .withOpacity(0.5) : Colors.black,
                                      )
                                          : Icon(
                                        Icons.visibility_off,
                                        size: (10 / 187.5) * screenWidth,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.5)),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [CustomColor.buttonTopBackgound, CustomColor
                              .buttonBottomBackgound
                          ],
                        ),
                      ),
                      child: Center(
                        child: Container(
                          height: double.infinity,
                          width: (107.7 / 187.5) * screenWidth,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.all(Radius.circular(30.5)),
                            color: CustomColor.button,
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(30.5)),
                            ),
                            color: CustomColor.button,
                            disabledColor: CustomColor.button,
                            onPressed: (isLoading) ? null : () {
                              onLoginButtonPressed();
                            },
                            child: asyncLoader,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment(1.0, 0),
                      child: Container(
                        height: (12 / 406) * screenHeight,
                        width: (59.7 / 187.5) * screenWidth,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(30.5)),
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              CustomColor.buttonTopBackgound,
                              CustomColor.buttonBottomBackgound
                            ],
                          ),
                        ),
                        child: Align(
                          alignment: Alignment(0.0, 0.0),
                          child: Container(
                            height: (10 / 406) * screenHeight,
                            width: (57.7 / 187.5) * screenWidth,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                              ),
                              color: CustomColor.button,
                              onPressed: () {
                                onForgotPasswordButtonPressed();
                              },
                              child: Text(
                                "Forgot Password",
                                style: TextStyle(
                                  fontSize: (5 / 187.5) * screenWidth,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getButtonContent() async {
    if (isLoading) {
      String password = textFieldController.text.toString();
      result = await login(email, password);
    }
  }

  onLoginButtonPressed() async {
    if (!isTextFieldEmpty) {
      setState(() {
        isLoading = true;
      });

      _asyncLoaderState.currentState
          .reloadState()
          .whenComplete(() {
        setState(() {
          isLoading = false;
        });
        if (result.status) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  LogoutScreen(
                    email: email,
                  ),
            ),
            ModalRoute.withName('/LoginScreen'),
          );
        } else {
          showAlert(context, result.statusMessage, null);
        }
      });
    }
    else {
      showAlert(context, "Please input password", null);
    }
  }

  onForgotPasswordButtonPressed() async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VerifyCodeScreen(
          email: email,
          isNewAccount: false,
        ),
      ),
    );
  }
}
