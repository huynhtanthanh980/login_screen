import 'dart:convert';

import 'package:http/http.dart' as http;

class LoginResult {
  final bool status;
  final String statusMessage;

  LoginResult({
    this.status,
    this.statusMessage,
  });

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
      status: json['status'],
      statusMessage: json['statusMessage'],
    );
  }
}

Future<LoginResult> login(String emailLogin, String password) async {
  final response = await http.post(
      'http://sanofi.codev.vn/api/user/auth?emailLogin=$emailLogin&passwordLogin=$password');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return LoginResult.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to login');
  }
}
