import 'dart:convert';

import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:login_screen/Utilities/CustomAlertDialog.dart';
import 'package:login_screen/Utilities/CustomColor.dart';

import 'main.dart';

class LogoutScreen extends StatefulWidget {
  final String email;

  LogoutScreen({Key key, @required this.email}) : super(key: key);

  @override
  State<LogoutScreen> createState() => LogoutScreenState(email);
}

class LogoutScreenState extends State<LogoutScreen> {
  String email;

  LogoutScreenState(this.email);

  final textFieldController = new TextEditingController();

  bool isLoading = false;

  final GlobalKey<AsyncLoaderState> _asyncLoaderState = new GlobalKey<
      AsyncLoaderState>();

  LogoutResult result;

  @override
  Widget build(BuildContext context) {
    var asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getButtonContent(),
      renderLoad: () =>
          Container(
            height: (9 / 406) * screenHeight,
            width: (9 / 406) * screenHeight,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
      renderError: ([error]) =>
      new Text('Error'),
      renderSuccess: ({data}) {
        return Text(
          "Log out",
          style: TextStyle(
            fontSize: (9 / 406) * screenHeight,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        height: screenHeight,
        width: screenWidth,
        child: Stack(
          children: <Widget>[
            Positioned(
              height: screenHeight,
              width: screenWidth,
              top: 0.0,
              child: Image.asset(
                'assets/RegisterScreen.png',
                fit: BoxFit.fill,
              ),
            ),
            Align(
              alignment: Alignment(0, 0),
              child: Container(
                height: (76.7 / 406) * screenHeight,
                width: (154.2 / 187.5) * screenWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: (20 / 187.5) * screenWidth,
                      child: CircleAvatar(
                        backgroundColor: CustomColor.textFieldBackgound,
                        child: Icon(
                          Icons.person,
                          size: (15 / 406) * screenHeight,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        email,
                        style: TextStyle(
                          fontSize: (6 / 187.5) * screenWidth,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.5)),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [CustomColor.buttonTopBackgound, CustomColor
                              .buttonBottomBackgound
                          ],
                        ),
                      ),
                      child: Center(
                        child: Container(
                          height: double.infinity,
                          width: (107.7 / 187.5) * screenWidth,
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                            color: CustomColor.button,
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30.5)),
                            ),
                            color: CustomColor.button,
                            disabledColor: CustomColor.button,
                            onPressed: (isLoading) ? null : () {
                              onRegisterButtonPressed();
                            },
                            child: asyncLoader,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getButtonContent() async {
    if (isLoading) {
      result = await logout(email);
    }
  }

  onRegisterButtonPressed() async {
    setState(() {
      isLoading = true;
    });

    _asyncLoaderState.currentState
        .reloadState()
        .whenComplete(() {
      setState(() {
        isLoading = false;
      });
      if (result.status) {
        //push Login Screen of pop out all the other route
        Navigator.pushNamedAndRemoveUntil(
            context, '/LoginScreen', ModalRoute.withName('/'));
      } else {
        showAlert(context, result.statusMessage, null);
      }
    });
  }
}

class LogoutResult {
  final bool status;
  final String statusMessage;

  LogoutResult({
    this.status,
    this.statusMessage,
  });

  factory LogoutResult.fromJson(Map<String, dynamic> json) {
    return LogoutResult(
      status: json['status'],
      statusMessage: json['statusMessage'],
    );
  }
}

Future<LogoutResult> logout(String email) async {
  final response =
      await http.post('http://sanofi.codev.vn/api/user/logout?email=$email');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return LogoutResult.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to log out');
  }
}
