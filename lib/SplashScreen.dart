import 'dart:async';

import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget{
  @override
  State<SplashScreen> createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {

  startTimer() async { //after x seconds, trigger navigationPage
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() { //Replace the current route of the navigator
    Navigator.of(context).pushReplacementNamed('/LoginScreen');
  }

  @override
  void initState() {
    super.initState();

    //start timer
    startTimer();
  }

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomPadding: false,

      body: Container(
        height: size.height,
        width: size.width,
        child: Image.asset(
          'assets/SplashScreen.png',
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}