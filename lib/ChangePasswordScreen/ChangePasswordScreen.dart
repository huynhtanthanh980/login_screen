import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:login_screen/ChangePasswordScreen/ChangePasswordViewModel.dart';
import 'package:login_screen/LoginScreen/PasswordLoginScreen.dart';
import 'package:login_screen/Utilities/CustomAlertDialog.dart';
import 'package:login_screen/Utilities/CustomColor.dart';

import '../main.dart';

class ChangePasswordScreen extends StatefulWidget {
  final String email;
  final String verifiedCode;

  ChangePasswordScreen({Key key, @required this.email, @required this.verifiedCode})
      : super(key: key);

  @override
  State<ChangePasswordScreen> createState() =>
      ChangePasswordScreenState(email, verifiedCode);
}

class ChangePasswordScreenState extends State<ChangePasswordScreen> {
  String email;
  final String verifiedCode;

  ChangePasswordScreenState(this.email, this.verifiedCode);

  final newPasswordTextFieldController = new TextEditingController();
  final rePasswordTextFieldController = new TextEditingController();

  var newPasswordObscureText = true;
  var rePasswordObscureText = true;

  var isNewPasswordTextFieldEmpty = true;
  var isRePasswordTextFieldEmpty = true;

  @override
  void initState() {
    super.initState();

    newPasswordTextFieldController.addListener(() {
      setState(() {
        if (newPasswordTextFieldController.text.isEmpty) {
          isNewPasswordTextFieldEmpty = true;
          newPasswordObscureText = true;
        }
        else {
          isNewPasswordTextFieldEmpty = false;
        }
      });
    });

    rePasswordTextFieldController.addListener(() {
      setState(() {
        if (rePasswordTextFieldController.text.isEmpty) {
          isRePasswordTextFieldEmpty = true;
          rePasswordObscureText = true;
        }
        else {
          isRePasswordTextFieldEmpty = false;
        }
      });
    });
  }

  bool isLoading = false;

  final GlobalKey<AsyncLoaderState> _asyncLoaderState = new GlobalKey<
      AsyncLoaderState>();

  PasswordChangeResult result;

  @override
  Widget build(BuildContext context) {
    var asyncLoader = new AsyncLoader(
      key: _asyncLoaderState,
      initState: () async => await getButtonContent(),
      renderLoad: () =>
          Container(
            height: (9 / 406) * screenHeight,
            width: (9 / 406) * screenHeight,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
      renderError: ([error]) =>
      new Text('Error'),
      renderSuccess: ({data}) {
        return Text(
          "Change Password",
          style: TextStyle(
            fontSize: (9 / 406) * screenHeight,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        height: screenHeight,
        width: screenWidth,
        child: Stack(
          children: <Widget>[
            Positioned(
              height: screenHeight,
              width: screenWidth,
              top: 0.0,
              child: Image.asset(
                'assets/RegisterScreen.png',
                fit: BoxFit.fill,
              ),
            ),
            Align(
              alignment: Alignment(0, 0),
              child: Container(
                height: (125.2 / 406) * screenHeight,
                width: (154.2 / 187.5) * screenWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: (20 / 187.5) * screenWidth,
                      child: CircleAvatar(
                        backgroundColor: CustomColor.textFieldBackgound,
                        child: Icon(
                          Icons.person,
                          size: (15 / 406) * screenHeight,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        email,
                        style: TextStyle(
                          fontSize: (6 / 187.5) * screenWidth,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30.5),
                                  bottomLeft: Radius.circular(30.5),
                                ),
                                color: CustomColor.textFieldBackgound,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    width: (6.3 / 187.5) * screenWidth,
                                  ),
                                  Icon(
                                    Icons.lock_outline,
                                    color: Colors.white,
                                    size: (10 / 187.5) * screenWidth,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(
                                      0.0,
                                      0.0,
                                    ),
                                    blurRadius: 10.0,
                                    spreadRadius: 0.0,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: <Widget>[
                                  TextField(
                                    controller: newPasswordTextFieldController,
                                    obscureText: newPasswordObscureText,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: (6 / 187.5) * screenWidth),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'New Password',
                                      hintStyle: TextStyle(
                                          fontSize: (6 / 187.5) * screenWidth),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment(0.9, 0.0),
                                    child: GestureDetector(
                                      onTapDown: (isNewPasswordTextFieldEmpty)
                                          ? null
                                          : (detail) {
                                        setState(() {
                                          newPasswordObscureText = false;
                                        });
                                      },
                                      onTapUp: (isNewPasswordTextFieldEmpty)
                                          ? null
                                          : (detail) {
                                        setState(() {
                                          newPasswordObscureText = true;
                                        });
                                      },
                                      onTapCancel: (isNewPasswordTextFieldEmpty)
                                          ? null
                                          : () {
                                        setState(() {
                                          newPasswordObscureText = true;
                                        });
                                      },
                                      child: newPasswordObscureText
                                          ? Icon(
                                        Icons.visibility,
                                        size: (10 / 187.5) * screenWidth,
                                        color: isNewPasswordTextFieldEmpty
                                            ? Colors.black.withOpacity(0.5)
                                            : Colors.black,
                                      )
                                          : Icon(
                                        Icons.visibility_off,
                                        size: (10 / 187.5) * screenWidth,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30.5),
                                  bottomLeft: Radius.circular(30.5),
                                ),
                                color: CustomColor.textFieldBackgound,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    width: (6.3 / 187.5) * screenWidth,
                                  ),
                                  Icon(
                                    Icons.replay,
                                    color: Colors.white,
                                    size: (10 / 187.5) * screenWidth,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(
                                      0.0,
                                      0.0,
                                    ),
                                    blurRadius: 10.0,
                                    spreadRadius: 0.0,
                                  ),
                                ],
                              ),
                              child: Stack(
                                children: <Widget>[
                                  TextField(
                                    controller: rePasswordTextFieldController,
                                    obscureText: rePasswordObscureText,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: (6 / 187.5) * screenWidth),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Re-New Password',
                                      hintStyle: TextStyle(
                                          fontSize: (6 / 187.5) * screenWidth),
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment(0.9, 0.0),
                                    child: GestureDetector(
                                      onTapDown: (isRePasswordTextFieldEmpty)
                                          ? null
                                          : (detail) {
                                        setState(() {
                                          rePasswordObscureText = true;
                                        });
                                      },
                                      onTapUp: (isRePasswordTextFieldEmpty)
                                          ? null
                                          : (detail) {
                                        setState(() {
                                          rePasswordObscureText = false;
                                        });
                                      },
                                      onTapCancel: (isRePasswordTextFieldEmpty)
                                          ? null
                                          : () {
                                        setState(() {
                                          rePasswordObscureText = false;
                                        });
                                      },
                                      child: rePasswordObscureText
                                          ? Icon(
                                        Icons.visibility,
                                        size: (10 / 187.5) * screenWidth,
                                        color: isRePasswordTextFieldEmpty
                                            ? Colors.black.withOpacity(0.5)
                                            : Colors.black,
                                      )
                                          : Icon(
                                        Icons.visibility_off,
                                        size: (10 / 187.5) * screenWidth,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.5)),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [CustomColor.buttonTopBackgound, CustomColor
                              .buttonBottomBackgound
                          ],
                        ),
                      ),
                      child: Center(
                        child: Container(
                          height: double.infinity,
                          width: (107.7 / 187.5) * screenWidth,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.all(Radius.circular(30.5)),
                            color: CustomColor.button,
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(30.5)),
                            ),
                            color: CustomColor.button,
                            disabledColor: CustomColor.button,
                            onPressed: () {
                              onChangePasswordButtonPressed();
                            },
                            child: asyncLoader,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getButtonContent() async {
    if (isLoading) {
      String password = newPasswordTextFieldController.text.toString();
      String repassword = rePasswordTextFieldController.text.toString();
      result = await changePassword(email, password, repassword, verifiedCode);
    }
  }

  onChangePasswordButtonPressed() async {
    String password = newPasswordTextFieldController.text.toString();
    String repassword = rePasswordTextFieldController.text.toString();

    if (isNewPasswordTextFieldEmpty) {
      showAlert(context, "Please input new password", null);
    } else if (isRePasswordTextFieldEmpty) {
      showAlert(context, "Please re-input new password", null);
    } else if (password != repassword) {
      showAlert(context, "Passwords don't match, try again ?", null);
    } else {
      setState(() {
        isLoading = true;
      });

      _asyncLoaderState.currentState
          .reloadState()
          .whenComplete(() {
        setState(() {
          isLoading = false;
        });

        if (result.status) {
          Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => PasswordLoginScreen(
                email: email,
              ),
            ),
            ModalRoute.withName('/LoginScreen'),
          );

          showAlert(context, 'Change Password Successful', null);
        } else {
          showAlert(context, result.statusMessage, null);
        }
      });
    }
  }
}
