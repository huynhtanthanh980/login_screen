import 'dart:convert';

import 'package:http/http.dart' as http;

class PasswordChangeResult {
  final bool status;
  final String statusMessage;

  PasswordChangeResult({
    this.status,
    this.statusMessage,
  });

  factory PasswordChangeResult.fromJson(Map<String, dynamic> json) {
    return PasswordChangeResult(
      status: json['status'],
      statusMessage: json['statusMessage'],
    );
  }
}

Future<PasswordChangeResult> changePassword(String email, String password,
    String repassword, String verifiedCode) async {
  final response = await http.post(
      'http://sanofi.codev.vn/api/user/changepassword?emailLogin=$email&passwordLogin=$password&passwordConfirmed=$repassword&verify_code=$verifiedCode');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return PasswordChangeResult.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to change password');
  }
}
