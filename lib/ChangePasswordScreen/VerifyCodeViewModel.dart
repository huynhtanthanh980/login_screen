import 'dart:convert';

import 'package:http/http.dart' as http;

class CodeVerifyResult {
  final bool status;
  final String statusMessage;

  CodeVerifyResult({
    this.status,
    this.statusMessage,
  });

  factory CodeVerifyResult.fromJson(Map<String, dynamic> json) {
    return CodeVerifyResult(
      status: json['status'],
      statusMessage: json['statusMessage'],
    );
  }
}

Future<CodeVerifyResult> verifyCode(String email, String code) async {
  final response = await http.post(
      'http://sanofi.codev.vn/api/user/checkcode?email=$email&verify_code=$code');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return CodeVerifyResult.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to verify code');
  }
}

class GetCodeResult {
  final bool status;
  final String statusMessage;
  final data;

  GetCodeResult({this.status, this.statusMessage, this.data});

  factory GetCodeResult.fromJson(Map<String, dynamic> json) {
    return GetCodeResult(
      status: json['status'],
      statusMessage: json['statusMessage'],
      data: json['data'],
    );
  }
}

Future<GetCodeResult> getCode(String email) async {
  final response =
      await http.post('http://sanofi.codev.vn/api/user/getcode?email=$email');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return GetCodeResult.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to verify code');
  }
}
