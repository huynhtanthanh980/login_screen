import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:login_screen/ChangePasswordScreen/ChangePasswordScreen.dart';
import 'package:login_screen/ChangePasswordScreen/VerifyCodeViewModel.dart';
import 'package:login_screen/Utilities/CustomAlertDialog.dart';
import 'package:login_screen/Utilities/CustomColor.dart';

import '../main.dart';

class VerifyCodeScreen extends StatefulWidget {
  final String email;
  final bool isNewAccount;

  VerifyCodeScreen({Key key, @required this.email, @required this.isNewAccount})
      : super(key: key);

  @override
  State<VerifyCodeScreen> createState() =>
      VerifyCodeScreenState(email, isNewAccount);
}

class VerifyCodeScreenState extends State<VerifyCodeScreen> {
  String email;
  bool isNewAccount;

  VerifyCodeScreenState(this.email, this.isNewAccount);

  final textFieldController = new TextEditingController();

  bool isLoading = false;
  bool isResend = false;

  final GlobalKey<AsyncLoaderState> _asyncVerifyLoaderState = new GlobalKey<
      AsyncLoaderState>();
  final GlobalKey<AsyncLoaderState> _asyncResendLoaderState = new GlobalKey<
      AsyncLoaderState>();

  CodeVerifyResult codeVerifyResult;
  GetCodeResult getCodeResult;

  @override
  Widget build(BuildContext context) {
    var asyncVerifyLoader = new AsyncLoader(
      key: _asyncVerifyLoaderState,
      initState: () async => await getVerifyButtonContent(),
      renderLoad: () =>
          Container(
            height: (9 / 406) * screenHeight,
            width: (9 / 406) * screenHeight,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
      renderError: ([error]) =>
      new Text('Error'),
      renderSuccess: ({data}) {
        return Text(
          "Verify",
          style: TextStyle(
            fontSize: (9 / 406) * screenHeight,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );

    var asyncResendLoader = new AsyncLoader(
      key: _asyncResendLoaderState,
      initState: () async => await getResendButtonContent(),
      renderLoad: () =>
          Container(
            height: (9 / 406) * screenHeight,
            width: (9 / 406) * screenHeight,
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
      renderError: ([error]) =>
      new Text('Error'),
      renderSuccess: ({data}) {
        return Text(
          "Resend code",
          style: TextStyle(
            fontSize: (7 / 406) * screenHeight,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        );
      },
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        height: screenHeight,
        width: screenWidth,
        child: Stack(
          children: <Widget>[
            Positioned(
              height: screenHeight,
              width: screenWidth,
              top: 0.0,
              child: Image.asset(
                'assets/RegisterScreen.png',
                fit: BoxFit.fill,
              ),
            ),
            Align(
              alignment: Alignment(0, 0),
              child: Container(
                height: (116.7 / 406) * screenHeight,
                width: (154.2 / 187.5) * screenWidth,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: (20 / 187.5) * screenWidth,
                      child: CircleAvatar(
                        backgroundColor: CustomColor.textFieldBackgound,
                        child: Icon(
                          Icons.person,
                          size: (15 / 406) * screenHeight,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        email,
                        style: TextStyle(
                          fontSize: (6 / 187.5) * screenWidth,
                        ),
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment(-1.0, 0.0),
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30.5),
                                  bottomLeft: Radius.circular(30.5),
                                ),
                                color: CustomColor.textFieldBackgound,
                              ),
                              child: Align(
                                alignment: Alignment(-0.9, 0),
                                child: Icon(
                                  Icons.vpn_key,
                                  color: Colors.white,
                                  size: (10 / 187.5) * screenWidth,
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              height: double.infinity,
                              width: (131.2 / 187.5) * screenWidth,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(
                                      0.0,
                                      0.0,
                                    ),
                                    blurRadius: 10.0,
                                    spreadRadius: 0.0,
                                  ),
                                ],
                              ),
                              child: TextField(
                                controller: textFieldController,
                                obscureText: false,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: (6 / 187.5) * screenWidth),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter Code',
                                  hintStyle: TextStyle(
                                      fontSize: (6 / 187.5) * screenWidth),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: (20 / 406) * screenHeight,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.5)),
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [CustomColor.buttonTopBackgound, CustomColor
                              .buttonBottomBackgound
                          ],
                        ),
                      ),
                      child: Center(
                        child: Container(
                          height: double.infinity,
                          width: (107.7 / 187.5) * screenWidth,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.all(Radius.circular(30.5)),
                            color: CustomColor.button,
                          ),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(30.5)),
                            ),
                            color: CustomColor.button,
                            disabledColor: CustomColor.button,
                            onPressed: (isLoading || isResend) ? null : () {
                              onRegisterButtonPressed();
                            },
                            child: asyncVerifyLoader,
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.0, 0.0),
                      child: Container(
                        height: (15 / 406) * screenHeight,
                        width: (89.7 / 187.5) * screenWidth,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(30.5)),
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              CustomColor.buttonTopBackgound,
                              CustomColor.buttonBottomBackgound
                            ],
                          ),
                        ),
                        child: Align(
                          alignment: Alignment(0.0, 0.0),
                          child: Container(
                            height: (15 / 406) * screenHeight,
                            width: (64.7 / 187.5) * screenWidth,
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.5)),
                              ),
                              color: CustomColor.button,
                              disabledColor: CustomColor.button,
                              onPressed: (isLoading || isResend) ? null : () {
                                onResendButtonPressed();
                              },
                              child: asyncResendLoader,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getVerifyButtonContent() async {
    if (isLoading) {
      String code = textFieldController.text.toString();
      codeVerifyResult = await verifyCode(email, code);
    }
  }

  onRegisterButtonPressed() async {
    String code = textFieldController.text.toString();

    if (code.isNotEmpty) {
      setState(() {
        isLoading = true;
      });

      _asyncVerifyLoaderState.currentState
          .reloadState()
          .whenComplete(() {
        setState(() {
          isLoading = false;
        });

        if (codeVerifyResult.status) {
          prefix0.Navigator.pop(context);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  ChangePasswordScreen(
                    email: email, verifiedCode: code,
                  ),
            ),
          );
        } else {
          showAlert(context, codeVerifyResult.statusMessage, null);
        }
      });
    }
    else {
      showAlert(context, "Please input code", null);
    }
  }

  getResendButtonContent() async {
    if (isResend) {
      getCodeResult = await getCode(email);
    }
  }

  onResendButtonPressed() async {
    if (isNewAccount) {
      showAlert(
          context, isNewAccount
          ? "Please check your email for the code"
          : "Code resent!", null);
    }
    else {
      setState(() {
        isResend = true;
      });

      _asyncResendLoaderState.currentState
          .reloadState()
          .whenComplete(() {
        textFieldController.text = getCodeResult.data.toString();
        showAlert(context, getCodeResult.data.toString(), null);

        setState(() {
          isResend = false;
        });
      });
    }
  }
}
